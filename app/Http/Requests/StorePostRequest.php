<?php

namespace App\Http\Requests;

use http\Env\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Client\Request;
use Nette\Schema\ValidationException;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'body' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Tên',
            'body' => 'Nội dung'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được để trống'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $reponse = response()->json([
            'statusCode' => \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Error validation',
            'errors' => $validator->errors()
        ], \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new \Illuminate\Validation\ValidationException($validator, $reponse));
    }
}
