<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function sentSuccessRespone($data = '', $message ='success', $status = 200)
    {
        return response()->json([
            'statusCode' => $status,
            'message' => $message,
            'data' => $data
        ], $status);
    }
}
