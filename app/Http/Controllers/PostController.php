<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;

class PostController extends Controller
{
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = $this->post->paginate(10);

        $postsCollection = new PostCollection($posts);

        return $this->sentSuccessRespone($postsCollection,'success', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        $dataCreate = $request->all();
        $post = $this->post->create($dataCreate);

        $postsResource = new PostResource($post);

        return $this->sentSuccessRespone($postsResource, 'success', Response::HTTP_CREATED);
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = $this->post->findOrFail($id);
        $postsResource = new PostResource($post);

        return $this->sentSuccessRespone($postsResource, 'success', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $post  = $this->post->findOrFail($id);
        $dataUpdate = $request->all();
        $post->update($dataUpdate);
        $postsResource = new PostResource($post);

        return $this->sentSuccessRespone($postsResource, 'success', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = $this->post->findOrFail($id);

        $post->delete();

        $postsResource = new PostResource($post);

        return $this->sentSuccessRespone($postsResource, 'success', Response::HTTP_NO_CONTENT);
    }
}
